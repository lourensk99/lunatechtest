'use strict';

describe('Controller: QueryCtrl', function () {

  // load the controller's module
  beforeEach(module('lunatechUiApp'));

  var QueryCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    QueryCtrl = $controller('QueryCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(QueryCtrl.awesomeThings.length).toBe(3);
  });
});
