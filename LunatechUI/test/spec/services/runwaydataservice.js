'use strict';

describe('Service: runwaydataservice', function () {

  // load the service's module
  beforeEach(module('lunatechUiApp'));

  // instantiate service
  var runwaydataservice;
  beforeEach(inject(function (_runwaydataservice_) {
    runwaydataservice = _runwaydataservice_;
  }));

  it('should do something', function () {
    expect(!!runwaydataservice).toBe(true);
  });

});
