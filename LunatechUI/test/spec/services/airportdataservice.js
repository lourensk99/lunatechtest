'use strict';

describe('Service: airportDataService', function () {

  // load the service's module
  beforeEach(module('lunatechUiApp'));

  // instantiate service
  var airportDataService;
  beforeEach(inject(function (_airportDataService_) {
    airportDataService = _airportDataService_;
  }));

  it('should do something', function () {
    expect(!!airportDataService).toBe(true);
  });

});
