'use strict';

describe('Service: countrydataservice', function () {

  // load the service's module
  beforeEach(module('lunatechUiApp'));

  // instantiate service
  var countrydataservice;
  beforeEach(inject(function (_countrydataservice_) {
    countrydataservice = _countrydataservice_;
  }));

  it('should do something', function () {
    expect(!!countrydataservice).toBe(true);
  });

});
