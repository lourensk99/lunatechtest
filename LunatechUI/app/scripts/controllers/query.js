'use strict';

/**
 * @ngdoc function
 * @name lunatechUiApp.controller:QueryCtrl
 * @description
 * # QueryCtrl
 * Controller of the lunatechUiApp
 */
angular.module('lunatechUiApp').controller('QueryCtrl',
  ['$scope', '$http', '$filter', 'countryDataService',
  function ($scope, $http, $filter, countryDataService) {

    $scope.displayed = [];
    $scope.isCode = false;

    $scope.callServer = function callServer(tableState) {

      $scope.isLoading = true;

      var pagination = tableState.pagination;

      var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
      var number = pagination.number || 10;  // Number of entries showed per page.

      if (typeof tableState.search.predicateObject !== 'undefined') {
        tableState.search.predicateObject.isCode = $scope.isCode;
      }

      console.log(tableState);
      countryDataService.getPage(start, number, tableState).then(function (result) {
        $scope.displayed = result.data;
        tableState.pagination.numberOfPages = result.totalRecords/number;//set the number of pages so the pagination can update
        tableState.pagination.totalItemCount = result.totalRecords;//set the number of pages so the pagination can update
        $scope.isLoading = false;
      });
    };

  }]);
