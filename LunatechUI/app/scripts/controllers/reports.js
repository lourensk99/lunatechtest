'use strict';

/**
 * @ngdoc function
 * @name lunatechUiApp.controller:ReportsCtrl
 * @description
 * # ReportsCtrl
 * Controller of the lunatechUiApp
 */
angular.module('lunatechUiApp')
  .controller('ReportsCtrl', ['$scope', 'airportDataService', 'runwayDataService', function ($scope, airportDataService, runwayDataService) {

    $scope.changeReport = function (report) {

      if (report === 'highest') {
        $scope.isLoading = true;
        airportDataService.getHighest10Report().then(function (result) {
          $scope.rowCollection = result;
          $scope.isLoading = false;
        });
      } else if (report === 'lowest') {
        $scope.isLoading = true;
        airportDataService.getLowest10Report().then(function (result) {
          $scope.rowCollection = result;
          $scope.isLoading = false;
        });
      } else if (report === 'typeRunway') {
        // types of runways per country
        $scope.displayed = [];
      } else if (report === 'topRunway') {
        $scope.isLoading = true;
        runwayDataService.commonRunways().then(function (result) {
          $scope.commonRunways = result;
          $scope.isLoading = false;
        });
      }

      $scope.activeReport = report;
    };

    $scope.callServer = function callServer(tableState) {

      $scope.isLoading = true;

      var pagination = tableState.pagination;

      var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
      var number = pagination.number || 10;  // Number of entries showed per page.

      airportDataService.getRunwaysPerCountry(start, number).then(function (result) {
        $scope.displayed = result.data;

        for (var dispInd in $scope.displayed) {
          var disp = $scope.displayed[dispInd];
          disp.runwayConcat = '';

          if (typeof disp.runways !== 'undefined') {

            for (var i = 0; i < disp.runways.length; i++) {
              var runway = disp.runways[i];
              disp.runwayConcat += runway;
              if (i !== (disp.runways.length - 1) && disp.runways.length > 1) {
                disp.runwayConcat += ', ';
              }
            }
          }
        }

        tableState.pagination.numberOfPages = result.totalRecords/number;//set the number of pages so the pagination can update
        tableState.pagination.totalItemCount = result.totalRecords;//set the number of pages so the pagination can update
        $scope.isLoading = false;
      });
    };

  }]);
