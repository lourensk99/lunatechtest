'use strict';

/**
 * @ngdoc service
 * @name lunatechUiApp.airportDataService
 * @description
 * # airportDataService
 * Factory in the lunatechUiApp.
 */
angular.module('lunatechUiApp')
  .factory('airportDataService', ['$http', function ($http) {

      var lunatechDataURL = 'http://localhost:8080/';

      function getHighest10Report() {
        var httpParam = {
          isDesc: true,
          limit: 10
        };

        return $http({
          method: 'POST',
          url: lunatechDataURL + 'countryAirportCount',
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          transformRequest: function(obj) {
            var str = [];
            for(var p in obj)
              str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
            return str.join('&');
          },
          data: httpParam
        }).then(function (res) {
          return res.data;
        });
      }

      function getLowest10Report() {
        var httpParam = {
          isDesc: false,
          limit: 10
        };

        return $http({
          method: 'POST',
          url: lunatechDataURL + 'countryAirportCount',
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          transformRequest: function(obj) {
            var str = [];
            for(var p in obj)
              str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
            return str.join('&');
          },
          data: httpParam
        }).then(function (res) {
          return res.data;
        });
      }

      function getRunwaysPerCountry(page, pageSize) {

        var httpParam = {
          pageStart: page,
          pageSize: pageSize
        };

        return $http({
          method: 'POST',
          url: lunatechDataURL + 'countryTypeRunwaysCount',
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          transformRequest: function(obj) {
            var str = [];
            for(var p in obj)
              str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
            return str.join('&');
          },
          data: httpParam
        }).then(function (res) {
          return res.data;
        });
      }

      return {
        getHighest10Report: getHighest10Report,
        getLowest10Report: getLowest10Report,
        getRunwaysPerCountry: getRunwaysPerCountry
      };
  }]);
