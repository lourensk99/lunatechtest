'use strict';

/**
 * @ngdoc service
 * @name lunatechUiApp.countryDataService
 * @description
 * # countrydataservice
 * Factory in the lunatechUiApp.
 */
angular.module('lunatechUiApp')
  .factory('countryDataService', ['$http', function ($http) {

    var lunatechDataURL = 'http://localhost:8080/';

    function getPage(page, pageSize, params) {
      var country = '';

      if (typeof params.search.predicateObject !== 'undefined' ? params.search.predicateObject.isCode : false) {
        country = params.search.predicateObject.code;
      } else {
        country = typeof params.search.predicateObject !== 'undefined' ? params.search.predicateObject.name : '';
      }

      if (typeof country === 'undefined') {
        country = '';
      }

      var httpParam = {
        country: country,
        isCode: typeof params.search.predicateObject !== 'undefined' ? params.search.predicateObject.isCode : false,
        pageStart: page,
        pageSize: pageSize
      };

      return $http({
        method: 'POST',
        url: lunatechDataURL + 'filterAirports',
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        transformRequest: function(obj) {
          var str = [];
          for(var p in obj)
            str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
          return str.join('&');
        },
        data: httpParam
      }).then(function (res) {
        return res.data;
      });
    }

    return {
      getPage: getPage
    };
  }]);
