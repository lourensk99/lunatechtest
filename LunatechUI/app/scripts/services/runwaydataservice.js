'use strict';

/**
 * @ngdoc service
 * @name lunatechUiApp.runwaydataservice
 * @description
 * # runwaydataservice
 * Factory in the lunatechUiApp.
 */
angular.module('lunatechUiApp')
  .factory('runwayDataService', ['$http', function ($http) {

    var lunatechDataURL = 'http://localhost:8080/';

    function commonRunways() {
      var httpParam = {
        isDesc: true,
        limit: 10
      };

      return $http({
        method: 'POST',
        url: lunatechDataURL + 'commonRunways',
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        transformRequest: function(obj) {
          var str = [];
          for(var p in obj)
            str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
          return str.join('&');
        },
        data: httpParam
      }).then(function (res) {
        return res.data;
      });
    }

    return {
      commonRunways: commonRunways
    };
  }]);
