# My project's README

This is the assessment:

##Download three CSV files from Amazon S3:##

* https://s3-eu-west-1.amazonaws.com/lunatechassessments/countries.csv

* https://s3-eu-west-1.amazonaws.com/lunatechassessments/airports.csv

* https://s3-eu-west-1.amazonaws.com/lunatechassessments/runways.csv


##Write a web application in Java or Scala that will ask the user for two actions : Query or Reports.##

###Query Option###

* Will ask the user for the country name or code and print the airports & runways at each airport. The input can be country code or country name. For bonus points make the test partial/fuzzy. e.g. entering zimb will result in Zimbabwe :)

###Choosing Reports will print the following:###

* 10 countries with highest number of airports (with count) and countries with lowest number of airports.

* Type of runways (as indicated in "surface" column) per country

* Bonus: Print the top 10 most common runway identifications (indicated in "le_ident" column)

*Feel free to use any library/framework as necessary but write it as a web application.

Please write the code as if you are writing production code, possibly with tests.

Spring

* http://www.journaldev.com/3358/spring-requestmapping-requestparam-pathvariable-example

AMAZON S3

* http://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/setup-project-gradle.html

* https://stackoverflow.com/questions/28568635/read-aws-s3-file-to-java-code

Open CSV

* http://opencsv.sourceforge.net/

* https://www.mkyong.com/java/how-to-read-and-parse-csv-file-in-java/

* https://agiletribe.wordpress.com/2012/11/23/the-only-class-you-need-for-csv-files/

* https://stackoverflow.com/questions/27094990/search-range-data-on-csv-file-using-java

* http://csvjdbc.sourceforge.net/

* http://commons.apache.org/proper/commons-csv/user-guide.html

REST Services

* https://spring.io/guides/gs/rest-service/

* https://spring.io/guides/gs/consuming-rest/#initial

AngularJS

* https://www.sitepoint.com/kickstart-your-angularjs-development-with-yeoman-grunt-and-bower/

* http://manuel.kiessling.net/2014/06/09/creating-a-useful-angularjs-project-structure-and-toolchain/

* https://github.com/resource/angular-starterkit

* http://lorenzofox3.github.io/smart-table-website/


Additional Interesting Finds

* https://jhipster.github.io/