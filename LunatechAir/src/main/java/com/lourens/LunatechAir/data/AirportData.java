package com.lourens.LunatechAir.data;

import com.lourens.LunatechAir.beans.Airport;
import com.lourens.LunatechAir.beans.Country;
import com.lourens.LunatechAir.beans.Runway;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AirportData {

    private static final Logger log = LoggerFactory.getLogger(AirportData.class);

    public static List<Country> countryList;
    public static List<Airport> airportList;
    public static List<Runway> runwayList;

    public static Map<String, Country> countryMap;
    public static Map<String, Airport> airportMap;
    public static Map<String, Runway> runwayMap;

    static {
        if (countryList == null || countryList.isEmpty()) {
            populateCountryFields();
        }

        if (airportList == null || airportList.isEmpty()) {
            populateAirportFields();
        }

        if (runwayList == null || runwayList.isEmpty()) {
            populateRunwayFields();
        }
    }

    public static void init() {};

    private static void populateCountryFields() {
        countryList = new ArrayList<>();
        RestTemplate restTemplate = new RestTemplate();
        String countriesCSV = restTemplate.getForObject("https://s3-eu-west-1.amazonaws.com/lunatechassessments/countries.csv", String.class);

        Reader in = new InputStreamReader(new ByteArrayInputStream(countriesCSV.getBytes()));

        try {
            Iterable<CSVRecord> records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(in);
            for (CSVRecord record : records) {
                Country country = new Country(record.get("id"),
                        record.get("code"),
                        record.get("name"),
                        record.get("continent"),
                        record.get("wikipedia_link"),
                        record.get("keywords"));

                countryList.add(country);

                if (countryMap == null || countryMap.isEmpty()) {
                    countryMap = new HashMap<>();
                }
                countryMap.put(country.getCode(), country);
            }
        } catch (IOException e) {
            log.error("Failed to read Countries from CSV", e);
        }
    }

    private static void populateAirportFields() {
        airportList = new ArrayList<>();
        RestTemplate restTemplate = new RestTemplate();
        String countriesCSV = restTemplate.getForObject("https://s3-eu-west-1.amazonaws.com/lunatechassessments/airports.csv", String.class);

        Reader in = new InputStreamReader(new ByteArrayInputStream(countriesCSV.getBytes()));

        try {
            Iterable<CSVRecord> records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(in);
            for (CSVRecord record : records) {
                Airport airport = new Airport(record.get("id"),
                        record.get("ident"),
                        record.get("type"),
                        record.get("name"),
                        record.get("latitude_deg"),
                        record.get("longitude_deg"),
                        "".equals(record.get("elevation_ft")) ? 0 : Integer.parseInt(record.get("elevation_ft")),
                        record.get("continent"),
                        record.get("iso_country"),
                        record.get("iso_region"),
                        record.get("municipality"),
                        Boolean.parseBoolean(record.get("scheduled_service")),
                        record.get("gps_code"),
                        record.get("iata_code"),
                        record.get("local_code"),
                        record.get("home_link"),
                        record.get("wikipedia_link"),
                        record.get("keywords"));

                airportList.add(airport);

                if (airportMap == null || airportMap.isEmpty()) {
                    airportMap = new HashMap<>();
                }
                airportMap.put(airport.getIdent(), airport);
            }
        } catch (IOException e) {
            log.error("Failed to read Countries from CSV", e);
        }
    }

    private static void populateRunwayFields() {
        runwayList = new ArrayList<>();
        RestTemplate restTemplate = new RestTemplate();
        String countriesCSV = restTemplate.getForObject("https://s3-eu-west-1.amazonaws.com/lunatechassessments/runways.csv", String.class);

        Reader in = new InputStreamReader(new ByteArrayInputStream(countriesCSV.getBytes()));

        try {
            Iterable<CSVRecord> records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(in);
            for (CSVRecord record : records) {
                Runway runway = new Runway(record.get("id"),
                        record.get("airport_ref"),
                        record.get("airport_ident"),
                        record.get("length_ft"),
                        record.get("width_ft"),
                        record.get("surface"),
                        record.get("lighted"),
                        record.get("closed"),
                        record.get("le_ident"),
                        record.get("le_latitude_deg"),
                        record.get("le_longitude_deg"),
                        record.get("le_elevation_ft"),
                        record.get("le_heading_degT"),
                        record.get("le_displaced_threshold_ft"),
                        record.get("he_ident"),
                        record.get("he_latitude_deg"),
                        record.get("he_longitude_deg"),
                        record.get("he_elevation_ft"),
                        record.get("he_heading_degT"),
                        record.get("he_displaced_threshold_ft"));

                runwayList.add(runway);

                if (runwayMap == null || runwayMap.isEmpty()) {
                    runwayMap = new HashMap<>();
                }
                runwayMap.put(runway.getId(), runway);
            }
        } catch (IOException e) {
            log.error("Failed to read Countries from CSV", e);
        }
    }
}
