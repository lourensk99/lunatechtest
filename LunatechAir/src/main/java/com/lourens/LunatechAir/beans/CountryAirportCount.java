package com.lourens.LunatechAir.beans;

/**
 * Created by lourens on 2017/06/03.
 */
public class CountryAirportCount extends Country {

    private long count;

    public CountryAirportCount(Country country, long count) {
        super(country.getId(), country.getCode(), country.getName(), country.getContinent(), country.getWikipedia_link(), country.getKeywords());
        this.count = count;
    }

    public CountryAirportCount(String id, String code, String name, String continent, String wikipedia_link, String keywords, long count) {
        super(id, code, name, continent, wikipedia_link, keywords);
        this.count = count;
    }

    public long getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
