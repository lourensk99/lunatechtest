package com.lourens.LunatechAir.beans;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by lourens on 2017/06/03.
 */
public class CountryTypeRunwaysCount extends Country {

    private Set<String> runways;

    public CountryTypeRunwaysCount(Country country, List<Runway> runways) {
        super(country.getId(), country.getCode(), country.getName(), country.getContinent(), country.getWikipedia_link(), country.getKeywords());
        if (this.runways == null) {
            this.runways = new TreeSet<>();
        }
        runways.forEach(runway -> {
            if (!"".equalsIgnoreCase(runway.getSurface())) {
                this.runways.add(runway.getSurface());
            }
        });
    }

    public CountryTypeRunwaysCount(String id, String code, String name, String continent, String wikipedia_link, String keywords, Set<Runway> runways) {
        super(id, code, name, continent, wikipedia_link, keywords);
        if (this.runways == null) {
            this.runways = new TreeSet<>();
        }
        runways.forEach(runway -> {
            if (!"".equalsIgnoreCase(runway.getSurface())) {
                this.runways.add(runway.getSurface());
            }
        });
    }

    public Set<String> getRunways() {
        return runways;
    }

    public void addRunway(String runway) {
        this.runways.add(runway);
    }

    public void addRunway(List<Runway> runways) {
        if (this.runways == null) {
            this.runways = new TreeSet<>();
        }
        runways.forEach(runway -> {
            if (!"".equalsIgnoreCase(runway.getSurface())) {
                this.runways.add(runway.getSurface());
            }
        });
    }
}
