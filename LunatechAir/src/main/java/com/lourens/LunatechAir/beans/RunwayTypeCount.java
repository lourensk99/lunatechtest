package com.lourens.LunatechAir.beans;

public class RunwayTypeCount {

    private String ident;
    private long count;

    public RunwayTypeCount(String ident, long count) {
        this.ident = ident;
        this.count = count;
    }

    public String getIdent() {
        return ident;
    }

    public void setIdent(String ident) {
        this.ident = ident;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
