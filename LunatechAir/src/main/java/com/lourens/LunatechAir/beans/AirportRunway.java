package com.lourens.LunatechAir.beans;

import java.util.List;

public class AirportRunway extends Airport{

    private List<Runway> runways;
    private String countryName;

    public AirportRunway(Airport airport, List<Runway> runways, String countryName) {
        super(airport.getId(), airport.getIdent(), airport.getType(), airport.getName(), airport.getLatitude_deg(), airport.getLongitude_deg(), airport.getElevation_ft(), airport.getContinent(), airport.getIso_country(), airport.getIso_region(), airport.getMunicipality(), airport.isScheduled_service(), airport.getGps_code(), airport.getIata_code(), airport.getLocal_code(), airport.getHome_link(), airport.getWikipedia_link(), airport.getKeywords());

        this.runways = runways;
        this.countryName = countryName;
    }
    public AirportRunway(String id, String ident, String type, String name, String latitude_deg, String longitude_deg, int elevation_ft, String continent, String iso_country, String iso_region, String municipality, boolean scheduled_service, String gps_code, String iata_code, String local_code, String home_link, String wikipedia_link, String keywords) {
        super(id, ident, type, name, latitude_deg, longitude_deg, elevation_ft, continent, iso_country, iso_region, municipality, scheduled_service, gps_code, iata_code, local_code, home_link, wikipedia_link, keywords);
    }

    public List<Runway> getRunways() {
        return runways;
    }

    public void setRunways(List<Runway> runways) {
        this.runways = runways;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}
