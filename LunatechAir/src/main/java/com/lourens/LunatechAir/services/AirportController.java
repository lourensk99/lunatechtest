package com.lourens.LunatechAir.services;

import com.lourens.LunatechAir.beans.*;
import com.lourens.LunatechAir.data.AirportData;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
public class AirportController {

    @CrossOrigin
    @PostMapping("/filterAirports")
    @ResponseBody
    public Map<String, Object> filterAirports(@RequestParam(value = "country", defaultValue = "") String country,
                                              @RequestParam(value = "isCode", defaultValue = "false") boolean isCode,
                                              @RequestParam(value = "pageStart", defaultValue = "0") int pageStart,
                                              @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {

        List<Country> filteredCountryList;
        if (isCode) {
            Stream<Country> stream = AirportData.countryList.stream();

            if (!"".equalsIgnoreCase(country)) {
                stream = stream.filter(c -> c.getCode().equalsIgnoreCase(country));
            }
            filteredCountryList = stream
                    .collect(Collectors.toList());
        } else {
            // get countries by name partial matched, return list to get country codes
            filteredCountryList = AirportData.countryList.stream()
                    .filter(c -> c.getName().toLowerCase().contains(country.toLowerCase()))
                    .collect(Collectors.toList());
        }

        List<AirportRunway> airportRunwayList = new ArrayList<>();

        Map<String, List<Runway>> groupByAirportMap = AirportData.runwayList.stream()
                .collect(Collectors.groupingBy(Runway::getAirport_ident));

        List<Airport> airports = AirportData.airportList.stream()
                .filter(p -> {
                    for (Country coun : filteredCountryList) {
                        if (p.getIso_country().equalsIgnoreCase(coun.getCode())) {
                            return true;
                        }
                    }
                    return false;
                }).sorted(Comparator.comparing(Airport::getName))
                .collect(Collectors.toList());

        airports.forEach(airport -> {
            Country country1 = AirportData.countryMap.get(airport.getIso_country());
            airportRunwayList.add(new AirportRunway(airport, groupByAirportMap.get(airport.getIdent()), country1.getName()));
        });

        int pageEnd = pageStart + pageSize;
        int toIndex = pageEnd > (airportRunwayList.size() - 1) ? (airportRunwayList.size() - 1) : pageEnd;
        Map<String, Object> response = new HashMap<>();

        response.put("totalRecords", airportRunwayList.size());
        response.put("data", airportRunwayList.subList(pageStart, toIndex < pageStart ? 0 : toIndex));

        return response;
    }

    @CrossOrigin
    @PostMapping("/countryAirportCount")
    @ResponseBody
    public List<CountryAirportCount> countryAirportCount(@RequestParam(value = "isDesc", defaultValue = "true") boolean isDesc,
                                                         @RequestParam(value = "limit", defaultValue = "10") long limit) {

        Map<String, Long> counting = AirportData.airportList.stream().collect(
                Collectors.groupingBy(Airport::getIso_country, Collectors.counting()));

        Map<String, Long> finalMap = new LinkedHashMap<>();

        // initialise comparator so it can be reversed or not depending on isDesc arg
        Comparator<Map.Entry<String, Long>> entryComparator = Map.Entry.comparingByValue();


        //Sort a map and add to finalMap
        counting.entrySet().stream()
                .sorted(isDesc ? entryComparator.reversed() : entryComparator)
                .limit(limit)
                .forEachOrdered(e -> finalMap.put(e.getKey(), e.getValue()));

        List<CountryAirportCount> countryCountList = new ArrayList<>();

        finalMap.forEach((String k, Long v) -> {
            Optional<Country> countryMatched = AirportData.countryList.stream().filter(p -> p.getCode().equalsIgnoreCase(k)).findFirst();

            countryMatched.ifPresent(country -> countryCountList.add(new CountryAirportCount(country, v)));
        });

        return countryCountList;
    }

    @CrossOrigin
    @PostMapping("/commonRunways")
    @ResponseBody
    public List<RunwayTypeCount> commonRunways(@RequestParam(value = "isDesc", defaultValue = "true") boolean isDesc,
                                                       @RequestParam(value = "limit", defaultValue = "10") long limit) {

        Map<String, Long> counting = AirportData.runwayList.stream().collect(
                Collectors.groupingBy(Runway::getLe_ident, Collectors.counting()));

        Comparator<Map.Entry<String, Long>> entryComparator = Map.Entry.comparingByValue();

        List<RunwayTypeCount> finalList = new ArrayList<>();

        //Sort a map and add to finalMap
        counting.entrySet().stream()
                .sorted(isDesc ? entryComparator.reversed() : entryComparator)
                .limit(limit)
                .forEachOrdered(e -> finalList.add(new RunwayTypeCount(e.getKey(), e.getValue())));

        return finalList;
    }

    @CrossOrigin
    @PostMapping("/countryTypeRunwaysCount")
    @ResponseBody
    public Map<String, Object> countryTypeRunwaysCount(@RequestParam(value = "pageStart", defaultValue = "0") int pageStart,
                                                       @RequestParam(value = "pageSize", defaultValue = "10") int pageSize) {

        List<CountryTypeRunwaysCount> runwayTypeList = new ArrayList<>();

        Map<String, CountryTypeRunwaysCount> runwayTypeMap = new HashMap<>();

        Map<String, List<Runway>> groupByAirportMap = AirportData.runwayList.stream()
                .collect(Collectors.groupingBy(Runway::getAirport_ident));

        AirportData.airportList.forEach(airport -> {
            List<Runway> runways = groupByAirportMap.get(airport.getIdent());

            if (runways != null) {
                Country country = AirportData.countryMap.get(airport.getIso_country());

                CountryTypeRunwaysCount countryTypeRunwaysCount = runwayTypeMap.get(country.getCode());
                if (countryTypeRunwaysCount == null) {
                    runwayTypeMap.put(country.getCode(), new CountryTypeRunwaysCount(country, runways));
                } else {
                    countryTypeRunwaysCount.addRunway(runways);
                }
            }
        });

        runwayTypeMap.entrySet().forEach(stringCountryTypeRunwaysCountEntry -> runwayTypeList.add(stringCountryTypeRunwaysCountEntry.getValue()));

        int pageEnd = pageStart + pageSize;
        int toIndex = pageEnd > (runwayTypeList.size() - 1) ? (runwayTypeList.size() - 1) : pageEnd;
        Map<String, Object> response = new HashMap<>();

        response.put("totalRecords", runwayTypeList.size());
        response.put("data", runwayTypeList.subList(pageStart, toIndex < pageStart ? 0 : toIndex));

        return response;
    }

}
