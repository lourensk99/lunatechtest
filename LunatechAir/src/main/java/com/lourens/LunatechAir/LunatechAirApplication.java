package com.lourens.LunatechAir;

import com.lourens.LunatechAir.data.AirportData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LunatechAirApplication {

    private static final Logger log = LoggerFactory.getLogger(LunatechAirApplication.class);

    public static void main(String[] args) {
        AirportData.init();
        SpringApplication.run(LunatechAirApplication.class, args);
    }

}